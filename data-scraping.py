import requests
from bs4 import BeautifulSoup
import pandas as pd

def extract(page):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36'}
    url = f'https://uk.indeed.com/jobs?q=python+developer&l=London,+Greater+London&start={page}'
    r = requests.get(url, headers)
    #return r.status_code
    soup = BeautifulSoup(r.content, "lxml")
    return soup
    

    
def transform(soup):
    div = soup.find_all('div', class_ ='job_seen_beacon')
    for item in div:
        title = item.find('span').text.strip()
        company = item.find('span', class_ = 'companyName').text.strip()
        try:
            salary = item.find('span', class_ = 'salary-snippet').text.strip()
        
        except:
            salary = ''
        summary = item.find('div', class_ = 'job-snippet').text.strip().replace('\n', '')
        
        job = {
            'title': title,
            'company' : company,
            'salary' : salary,
            'summary': summary
        }
        joblist.append(job)
    return
    

joblist = []

for i in range(0, 40, 10):
    print(f'Get page, {i}')
    c  = extract(0)
    transform(c)
    
    
df = pd.DataFrame(joblist)
print(df.head())
df.to_csv('job.csv')



    
